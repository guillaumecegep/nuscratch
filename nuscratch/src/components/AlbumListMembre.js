import React  from 'react';
// import SearchBox from '../components/SearchBox';
import ErrorBoundry from '../components/ErrorBoundry';
import CardListAlbum from './CardListAlbum';

class AlbumsListMembre extends React.Component {
  constructor(props){
    super(props)
    this.state = {
        albums: [],
        searchfield:''
    };
    // console.log("id")
    // console.log(props.userIdAlbum)
  }
  componentDidMount(props){
    // console.log("props")
    // console.log(props)
    fetch(`https://jsonplaceholder.typicode.com/albums?userId=${4}`)
      .then(response =>{
        return response.json()
      })
      .then(
        title=>{this.setState({albums: title})}   
      );    
    }
    onSearchChange = (e) => {
      this.setState({searchfield : e.target.value});
    }

    render(){
      const {albums,searchfield} = this.state;
      const filteredAlbums = albums.filter(album =>{
        return album.title.toLowerCase().includes(searchfield.toLowerCase())
      })

      if (!albums.length){
        return (
          <div className="albums-containers">
            <div className="spinner-border" role="status">
              <h2 className="sr-only">Chargement...</h2>
            </div>
         </div>
        )
      }
      else{
        return(          
          <div className="albums-containers-membre">
              {/* <SearchBox searchChange={this.onSearchChange}/> */}
              <ErrorBoundry> 
                  <CardListAlbum albums={filteredAlbums}/>
              </ErrorBoundry>
          </div>
          );
        }
      }
    }
export default AlbumsListMembre;