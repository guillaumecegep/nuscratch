import React from 'react';


const AlbumLayout = ({title,id}) =>{
    return(
        <div className="album-layout">
            {              
              <div>
              <h1>{title}</h1>
              <h2>{id}</h2></div>   
            }            
        </div>
    );
}

export default AlbumLayout;

