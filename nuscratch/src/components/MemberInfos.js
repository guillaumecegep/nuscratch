import React from 'react';

// Information du membre

const MemberInfos = (props) =>{
    return(        
        <>          
             <div className="membre-layout-info">
                <h4>Informations</h4>
                <div className="membre-layout-info-perso">
                    <ul>
                        <li>{props.name}</li>
                        <li>Courriel : <span>{props.email}</span></li>
                    </ul>
                </div>
                <div className="membre-layout-info-address">
                    <h6>Adresse</h6>
                    <ul>
                        <li>{props.street}, {props.suite}</li>
                        <li>{props.city}</li>
                        <li>{props.zipcode}</li>
                    </ul>
                </div>
            </div>
        </>
        
    );
}

export default MemberInfos;