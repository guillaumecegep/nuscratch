import React from 'react';

const SearchBox = ({searchfield, searchChange}) =>{
    return (
        <div className="search-container">
            <input 
            className="searchfield"
            type ='search'
            placeholder="Rechercher..."
            onChange={searchChange}
            />
        </div>
    );
}
export default SearchBox;