import React from 'react';
import LinkButton from './LinkButton';
import MemberInfos from './MemberInfos';
import AlbumsListMembre from './AlbumListMembre';
import ErrorBoundry from './ErrorBoundry';


// Liste des membres 

const FicheMemberLayout = (props) =>{
    return(
        <div className="membre-layout">                     
            <div className="membre-layout-side">
                <div className="profil-pic">
                    <img alt="profil pic" src={`http://i.pravatar.cc/600?img=${props.id}`} />
                    <LinkButton to='#'>Proposer un échange</LinkButton>
                </div>  
              <ErrorBoundry>
                <MemberInfos     
                        key={props.id} 
                        id={props.id} 
                        name={props.name} 
                        email={props.email}
                        tel={props.website}
                        street={props.street}
                        suite={props.suite}
                        zipcode={props.zipcode}
                        city={props.city}
                        />
                </ErrorBoundry>
            </div>
            <div className="membre-layout-albums">
                <h4>Albums en échange</h4>
                <ErrorBoundry>
                    <AlbumsListMembre userIdAlbum = {props.id}/>
                </ErrorBoundry>
            </div>
        </div>
    );
}

export default FicheMemberLayout;

