import React from 'react';

// Erreur visiteur

class ErrorBoundry extends React.Component{
    constructor(props){
        super(props);
        this.state={
            hasError : false,
            error: null,
            errorInfo: null,
        }
    }
    componentDidCatch(error, errorInfo){
            this.setState({hasError : true, error: error, errorInfo: errorInfo});
            console.log(error, errorInfo);        
    }
    render(){
        if(this.state.hasError){
            return <h1> Aie! Aie! Aie! Quelque chose s'est mal passé!</h1>
        }       
        return this.props.children
    }
}
export default ErrorBoundry;