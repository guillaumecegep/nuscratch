//http://jerairrest.github.io/react-chartjs-2/  
import React from 'react'
import {Doughnut} from 'react-chartjs-2';
import ErrorBoundry from './ErrorBoundry';


  const ChartDoughnut = (props) =>{
  const resussi = props.reussi;
  const echec = props.echec;
  const data = {
    labels: [
      'Échanges complétés',
      'Échanges non-complétés',
      {
        "display": false,
        "position": "top",
        "fullWidth": true,
        "reverse": false,
        "labels": {
          "fontColor": "rgb(255, 99, 132)"
        }
      }       
    ],
    datasets: [{
      data: [resussi, echec],
      backgroundColor: [
      '#bd4547',
      '#17a2b8'      
      ],
      hoverBackgroundColor: [
      '#dc3545',
      '#007bff'        
      ]
    }], 
  };

  return (
    <div>
      <ErrorBoundry>
        <Doughnut data={data} 
                  width={80}
                  height={80}
                  legend={{display: false}}
                  options={{ maintainAspectRatio: true }}
        />
      </ErrorBoundry>
    </div>
  )

  }
export default ChartDoughnut


