import React from 'react';
import LinkButton from './LinkButton';

// Cards des membres

const CardMember = (props) =>{
    return(        
        <div className="cards-container membres">
            <div className="cards membres" key={props.id}>
                <img className="cards-img-top membres" alt="member pic" src={`http://i.pravatar.cc/600?img=${props.id}`}/>
                <div className="cards-info membres">
                    <h4>{props.name}</h4>
                    <p>{props.bs}</p>
                    <p>{props.email}</p>
                </div>
                <div className="cards-footer membres">
                    <LinkButton className="btn-auto"to={{pathname:`/Membres/${props.name}`, state:{id:props.id}}}>Albums un échange</LinkButton>
                </div>
            </div>
        </div>
    );
}

export default CardMember;