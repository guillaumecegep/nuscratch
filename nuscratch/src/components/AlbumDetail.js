import React from 'react'
import ErrorBoundry from '../components/ErrorBoundry';
import AlbumLayout from './AlbumLayout';


class AlbumDetail extends React.Component{
    constructor(){
        super()
        this.state ={  
            albums: []
        };
    }
    componentDidMount(){        
        const id = this.props.location.state.id;
        var albumFetch = `https://jsonplaceholder.typicode.com/albums/`+ id;
        console.log(albumFetch);
        fetch(albumFetch)
          .then(response =>{
            return response.json()
          })
          .then(
            title=>{this.setState({albums: title})}   
          );        
    }
    render(){
        const {albums} = this.state;        
        if (!albums){
            return (            
                <div className="albums-containers">
                    <h1>Détail de l'album : </h1>
                    <div className="spinner-border" role="status">
                        <h2 className="sr-only">Chargement...</h2>
                    </div>
                </div>             
            )              
        }
        else{
            return(
            <div className="albums-details-containers">
                <div className="album">              
                    <ErrorBoundry>                     
                        <AlbumLayout key={albums.id} id={albums.id} title={albums.title} userId={albums.userId}  />                  
                    </ErrorBoundry>
                </div>
            </div>
            );
        } 
    }
}

export default AlbumDetail