// https://codesandbox.io/s/8xr92yn2ol
import React from "react";
import { Link } from "react-router-dom";
import withBreadcrumbs from "react-router-breadcrumbs-hoc";

const PureBreadcrumbs = ({ breadcrumbs }) => (
  <div className="breadcrumb-container">
    <nav className="breadcrumbs  container" aria-label="breadcrumb">
      <ol className="breadcrumb">
        {breadcrumbs.map(({ breadcrumb, match }, index) => (
        <li className="bc breadcrumb-item" key={match.url}>
        <Link to={match.url || ""}>{breadcrumb}</Link>
        {index < breadcrumbs.length - 1}
        </li>
        ))}
      </ol>
    </nav>
  </div>
);

export default withBreadcrumbs()(PureBreadcrumbs);