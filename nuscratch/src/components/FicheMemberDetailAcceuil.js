import React from 'react'
import FicheMemberLayoutAcceuil from './FicheMemberLayoutAcceuil';
import ErrorBoundry from './ErrorBoundry';


class FicheMemberDetailAcceuil extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            isLoaded:false,
            membres: []          
        };
    }
    componentDidMount(){
        const id = this.props.id;
        var membreFetch = `https://jsonplaceholder.typicode.com/users/`+ id;
        // console.log("membre fetch "+ membreFetch);
        fetch(membreFetch)
            .then(response =>{
            return response.json()
            })
            .then(
            name=>{this.setState({
                membres: name,
                isLoaded:true
            })}              
        ) 
    }
    render(){
        const {membres} = this.state; 
        // console.log("membres " + membres);
        let testLoadState;
        if (this.state.isLoaded) {
            testLoadState = 
                <ErrorBoundry>
                    <FicheMemberLayoutAcceuil 
                        key={membres.id} 
                        id={membres.id} 
                        name={membres.name} 
                        email={membres.email}
                        tel={membres.phone}
                        street={membres.address.street}
                        suite={membres.address.suite}
                        zipcode={membres.address.zipcode}
                        city={membres.address.city} 
                        />     
                </ErrorBoundry>    
        } else {
            testLoadState = <ErrorBoundry> <FicheMemberLayoutAcceuil /></ErrorBoundry>;
        }        
        if (!membres){
            return (            
                <div className="membres-containers">
                    <h1>Détails du membre : </h1>
                    <div className="spinner-border" role="status">
                        <h2 className="sr-only">Chargement...</h2>
                    </div>
                </div>             
              )              
        }
        else{
       
            return(
            <div className="membres-details-containers">
                <h1>{membres.username} </h1>
                <div className="membre">              
                    {testLoadState}            
                </div>
            </div>
            );
        } 
    }
}






// const membreDetail = (props) => {
//     const membre = props ;
 
//     if (!membre){
//         return <div> Désolé, pas d'membre correspondant</div>
//     }
//     return(
//         <div>

//             <h2>{props.match.params.id}</h2>
//             <h2>{props.match.params.title}</h2>

//         </div>
//     )

// }

export default FicheMemberDetailAcceuil