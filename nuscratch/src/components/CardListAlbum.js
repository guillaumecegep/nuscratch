import React from 'react';
import Card from './Card';


const CardListAlbum = ({albums}) =>{
    return(
        <div className="card-list">
            {
                albums.map((title, i)=>{
                    return(
                        <Card
                            key={i}
                            id={albums[i].id}
                            userId={albums[i].userId}
                            title= {albums[i].title}                            
                        />
                    );
                })
            }
        </div>
    );
}

export default CardListAlbum;