import React from 'react';
import {Link} from 'react-router-dom';

const Card = ({title, id, userId}) =>{
    return(        
        <div className="card-container album">
            <div className="cards album">
                <img alt="album cover" src={`https://loremflickr.com/200/200/cdcover?random?`}/>
                <div className="cards-info-album">
                    <h6>{title}</h6>
                </div>
                <div className="cards-footer">
                    <Link to={{pathname:`/Membres/${userId}`, state:{id:userId}}}>fiche du membre</Link>
                </div>
            </div>
        </div>
        
    );
}

export default Card;