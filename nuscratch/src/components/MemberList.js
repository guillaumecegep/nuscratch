import React  from 'react';
import CardListMember from './CardListMember';
import SearchBox from './SearchBox';
import ErrorBoundry from './ErrorBoundry';

class MembersList extends React.Component  {
  constructor(){
    super()
    this.state = {
        membres: [],
        searchfield:''
    };        
}
  componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(response =>{
        return response.json()
      })
      .then(
        name=>{this.setState({membres: name})}   
      );
    
  }
  onSearchChange = (e) => {
    this.setState({searchfield : e.target.value});
  }
  render(){
    const {membres,searchfield} = this.state;
    const filteredMembres = membres.filter(membre =>{
      return membre.name.toLowerCase().includes(searchfield.toLowerCase())
    })
    if (!membres.length){
      return (
        <div className="membres-containers">
          <h1>Liste des membres</h1>
          <div className="spinner-border" role="status">
            <h2 className="sr-only">Chargement...</h2>
          </div>
        </div>
      )
    }
    else{
      return(          
        <div className="membres-containers">
          <h1>Listes des membres</h1>
            <SearchBox searchChange={this.onSearchChange}/>
              <ErrorBoundry> 
                  <CardListMember membres={filteredMembres}/>
              </ErrorBoundry>
        </div>
      );
    }
  }
}
export default MembersList;