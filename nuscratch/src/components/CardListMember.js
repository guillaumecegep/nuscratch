import React from 'react';
import CardMember from './CardMember';

// Liste des membres 

const CardListMember = ({membres}) =>{
    return(
        <div className="cards-list membres">
            {
                membres.map((name, i)=>{
                    return(
                        <CardMember
                            key={i}
                            id={membres[i].id}
                            bs={membres[i].company.bs}
                            email={membres[i].email}
                            name={membres[i].name}                            
                        />
                    );
                })
            }
        </div>
    );
}

export default CardListMember;