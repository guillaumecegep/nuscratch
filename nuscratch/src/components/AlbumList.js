import React  from 'react';
import SearchBox from '../components/SearchBox';
import ErrorBoundry from '../components/ErrorBoundry';
import CardListAlbum from './CardListAlbum';

class AlbumsList extends React.Component {
  constructor(){
    super()
    this.state = {
        albums: [],
        searchfield:''
    };
  }

  componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/albums')
      .then(response =>{
        return response.json()
      })
      .then(
        title=>{this.setState({albums: title})}   
      );    
}
    onSearchChange = (e) => {
      this.setState({searchfield : e.target.value});
    }

    render(){
      const {albums,searchfield} = this.state;
      const filteredAlbums = albums.filter(album =>{
        return album.title.toLowerCase().includes(searchfield.toLowerCase())
      })

      if (!albums.length){
        return (
          <div className="albums-containers">
            <h1>Albums en échange</h1>
            <div className="spinner-border" role="status">
              <h2 className="sr-only">Chargement...</h2>
            </div>
         </div>
        )
      }
      else{
        return(          
          <div className="albums-containers">
            <h1>Albums en échange</h1>
              <SearchBox searchChange={this.onSearchChange}/>
              <ErrorBoundry> 
                  <CardListAlbum albums={filteredAlbums}/>
              </ErrorBoundry>
            </div>
          );
        }
      }
    }
export default AlbumsList;