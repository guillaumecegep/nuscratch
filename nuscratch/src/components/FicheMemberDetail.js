import React from 'react'
import FicheMemberLayout from './FicheMemberLayout';
import ErrorBoundry from './ErrorBoundry';

class FicheMemberDetail extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            isLoaded:false,
            membres: []          
        };
    }
    componentDidMount(){
        const id = this.props.location.state.id;
        var membreFetch = `https://jsonplaceholder.typicode.com/users/`+ id;
        // console.log("membre fetch "+ membreFetch);
        fetch(membreFetch)
          .then(response =>{
            return response.json()
          })
          .then(
            name=>{this.setState({
                membres: name,
                isLoaded:true
            })}              
        )  
    }

    render(){
        const {membres} = this.state; 
        // console.log("membres " + membres);
        let testLoadState;
        if (this.state.isLoaded) {
            testLoadState =
                <ErrorBoundry> 
                    <FicheMemberLayout 
                        key={membres.id} 
                        id={membres.id} 
                        name={membres.name} 
                        email={membres.email}
                        tel={membres.phone}
                        street={membres.address.street}
                        suite={membres.address.suite}
                        zipcode={membres.address.zipcode}
                        city={membres.address.city} 
                        /> 
                </ErrorBoundry>
        } 
        else {
            testLoadState = <FicheMemberLayout />;
        }
        if (!membres){
            return (            
                <div className="membres-containers">
                    <h1>Détails du membre : </h1>
                    <div className="spinner-border" role="status">
                        <h2 className="sr-only">Chargement...</h2>
                    </div>
                </div>             
            )              
        }
        else{       
            return(
            <div className="membres-details-containers">
                <h1>{membres.username} </h1>
                <div className="membre">              
                    {testLoadState}            
                </div>
            </div>
            );
        } 
    }
}
export default FicheMemberDetail