import React  from 'react';
import ChartDoughnut from '../components/ChartDoughnut';
import ErrorBoundry from './ErrorBoundry';

class ChardData extends React.Component {
  constructor(){
    super()
    this.state = {
        Data:[],
        isLoaded:false,
        error:null       
    };
  }
  componentDidMount(){
    fetch('https://jsonplaceholder.typicode.com/todos?userId=10')
      .then(response =>{
        console.log(response)
        return response.json()
      })
      .then(
        (resultats)=>{          
          let reussi = 0;
          let echec = 0;

          for (let i = 0; i < resultats.length; i++) {
            if (resultats[i].completed === true){
              reussi++;
            }
            else{
              echec++;
            }  
          }
          //console.log("reussi " + reussi);
          //console.log("echec " + echec);

      this.setState({
        isLoaded:true,
        Data:resultats,
        reussi:reussi,
        echec:echec
      });  
    },
    (error)=>{
      this.setState({
        isLoaded:true,
        error
      });
    }
  )
}
    render(){
      const {reussi, echec} = this.state;
      let testLoadState;
      if (this.state.isLoaded) {
          testLoadState = 
          <ErrorBoundry>
            <ChartDoughnut reussi={reussi} echec={echec}/>
          </ErrorBoundry>
      } 
      else {
          testLoadState = <ErrorBoundry><ChartDoughnut /></ErrorBoundry>;
      }      
      return(          
        <div className="chart-container">
          {testLoadState}
        </div>
      )
  }
}
export default ChardData;