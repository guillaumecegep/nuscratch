import React, { Component } from 'react'
import {Bar} from 'react-chartjs-2';
import ErrorBoundry from './ErrorBoundry';


export class ChartBar extends Component {
  render() {
    const data = {
        labels: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
        datasets: [
          {
            label: 'Échanges complétés par mois',
            backgroundColor: 'rgba(189, 69, 71,0.2)',
            borderColor: 'rgba(189, 69, 71,1)',
            borderWidth: 1,
            hoverBackgroundColor: 'rgba(220, 53, 69,0.4)',
            hoverBorderColor: 'rgba(220, 53, 69,1)',
            data: [5, 10, 15, 21, 26, 25, 25,3,32,20,10,12]
          }
        ]
      };    
    return (
        <div>
            <ErrorBoundry>
              <Bar
                data={data}
                width={100}
                height={50}
                options={{
                    maintainAspectRatio: true
                }}
                legend={{display: false}}
              />
            </ErrorBoundry>
        </div>
    )
  }
}

export default ChartBar
