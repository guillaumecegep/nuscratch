import React from 'react';
import MemberInfos from './MemberInfos';
import ErrorBoundry from './ErrorBoundry';


// Liste des membres 

const FicheMemberLayoutAcceuil = (props) =>{
    return(
        <div className="membre-layout">                     
            <div className="membre-layout-side">
                <ErrorBoundry>
                    <MemberInfos     
                            key={props.id} 
                            id={props.id} 
                            name={props.name} 
                            email={props.email}
                            tel={props.phone}
                            street={props.street}
                            suite={props.suite}
                            zipcode={props.zipcode}
                            city={props.city}
                            />
                </ErrorBoundry>
            </div>           
        </div>
    );
}

export default FicheMemberLayoutAcceuil;

