import React from 'react'
import {Switch, Route} from 'react-router-dom'
import AlbumsList from '../components/AlbumList'
import AlbumDetail from '../components/AlbumDetail'

const MainAlbums = () => (
  <Switch>
    <Route exact path = '/Albums' component = {AlbumsList}/>
    <Route path='/Albums/:id' component = {AlbumDetail}/>
  </Switch>
)

export default MainAlbums
