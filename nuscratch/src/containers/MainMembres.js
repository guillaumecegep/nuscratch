import React from 'react'
import {Switch, Route} from 'react-router-dom'
import MembersList from '../components/MemberList'
import FicheMemberDetail from '../components/FicheMemberDetail'

const MainMembres = () => (
  <Switch>
    <Route exact path = '/Membres' component = {MembersList}/>
    <Route path='/Membres/:id' component = {FicheMemberDetail}/>
  </Switch>
)

export default MainMembres
