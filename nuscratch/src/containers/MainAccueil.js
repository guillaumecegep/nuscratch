import React, { Component } from 'react'
import ChartData from '../components/ChartData';
import ChartBar from '../components/ChartBar';
import FicheMemberDetailAcceuil from '../components/FicheMemberDetailAcceuil'
import LinkButton from '../components/LinkButton'

export class MainAccueil extends Component {
  render() {
    return (
      <div className="main-acceuil">
        <div className="main-acceuil-titre">
          <h1>Mon profil</h1>      
          <button className="btn logout btn-secondary my-2 my-sm-0" type="submit">Déconnexion</button>
        </div>
        <div className="profil-dashboard-container">
          <div className="profil-gestion">
            <h3>Gestion</h3>
            <div className="profil-content">
              <LinkButton to='#'>Gérer mes albums</LinkButton>
              <LinkButton to={{pathname:`/Membres/${10}`, state:{id:10}}}>Voir l'historique des échanges</LinkButton>
              <LinkButton to={{pathname:`/Membres/${10}`, state:{id:10}}}>Gérer les livraisons</LinkButton>              
            </div>
          </div>
          <div className="profil-info">
            <FicheMemberDetailAcceuil id={10} />
          </div>
          <div className="profil-pic">
            <img alt="profil pic" src="http://i.pravatar.cc/600?img=10" />
          </div>
        </div>
        <div className="profil-charts-container">
          <div className="profil-echange">
            <h4>Echanges complétés par mois</h4>
              <ChartBar/>            
          </div>
          <div className="profil-succes">
            <h4>Taux de succès des échanges</h4>            
              <div className="chart-container">
                <ChartData />
              </div>    
          </div>         
        </div>
      </div>
    )
  }
}

export default MainAccueil
