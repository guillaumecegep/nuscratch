import React from 'react';
import './App.scss';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import HeaderBottom from './HeaderBottom'

const App = () => { 
  return (
    <div>
      <Header />
      <Main />
      <HeaderBottom />
      <Footer />
    </div>   
  )
}

export default App;