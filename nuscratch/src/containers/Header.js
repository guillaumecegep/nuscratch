import React from 'react'
import { Link } from 'react-router-dom'

// The Header creates links that can be used to navigate
// between routes.
const Header = () => (
  <header>
    <nav className="nav-main-top container">
      <Link className="nav-logo" to='/'>NuScratch</Link> 
    
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="">
          <li className="">
            <Link className="nav-link" to='/'>Mon Profil</Link>
          </li>
          <li className="">
            <Link className="nav-link" to='/Membres'>Membres</Link>
          </li>
          <li className="">
            <Link className="nav-link" to='/Albums'>Albums</Link>
          </li>
        </ul>
      </div>
      <button className="btn logout btn-secondary my-2 my-sm-0" type="submit">Déconnexion</button>

    </nav>
  </header>
)

export default Header

// <nav>
// <ul>
//   <li><Link to='/'>Mon Profil</Link></li>
//   <li><Link to='/Albums'>Albums</Link></li>
//   <li><Link to='/Membres'>Membres</Link></li>
// </ul>
// </nav>