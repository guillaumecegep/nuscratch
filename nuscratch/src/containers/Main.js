import React from 'react'
import { Switch, Route } from 'react-router-dom'
import MainAccueil from './MainAccueil';
import MainAlbums from './MainAlbums';
import MainMembres from './MainMembres';
import FicheMemberDetail from '../components/FicheMemberDetail';
import AlbumDetail from '../components/AlbumDetail';
import Breadcrumbs from '../components/Breadcrumbs'

const Main = () => (
  <main>
    <Breadcrumbs/>
    <div className="container">
      <Switch>
        <Route exact path='/' component={MainAccueil}/>
        <Route path='/Albums' exact component={MainAlbums}/>
        <Route path='/Albums/:id' exact component={AlbumDetail} />
        <Route path='/Membres' component={MainMembres}/>
        <Route path='/Membres/:id' exact component={FicheMemberDetail} />
      </Switch>
    </div>
  </main>
)

export default Main