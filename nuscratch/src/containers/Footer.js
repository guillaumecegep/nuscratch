import React, { Component } from 'react'

export class Footer extends Component {
  render() {
    return (      
        <footer className="page-footer">        
          <div className="footer-copyright text-center py-3">Développé par <i>Guillaume Ernst</i> avec <i>React</i>          
          </div>  
        </footer>      
    )
  }
}

export default Footer
