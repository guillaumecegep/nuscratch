import React from 'react'
import { Link } from 'react-router-dom'

// The Header creates links that can be used to navigate
// between routes.
const HeaderBottom = () => (
  
    <div className="header-bottom">
        <nav className="nav-main container">
            <div className="nav-mobile bottom" id="navbarSupportedContent">
                <ul className="">
                    <li className="">
                        <Link className="nav-link bottom" to='/'>
                            <img alt="profil pic" src="http://i.pravatar.cc/100?img=10" />
                            <span>
                                Mon Profil
                            </span>
                        </Link>
                    </li>
                    <li className="">
                        <Link className="nav-link bottom" to='/Membres'>
                        <i className="fa fa-users"></i>
                        <span>Membres</span></Link>
                    </li>
                    <li className="">
                        <Link className="nav-link bottom" to='/Albums'>
                        <i className="fa fa-music"></i>
                        <span>Albums</span></Link>
                    </li>
                </ul>           
            </div>
        </nav>
    </div>
  
)

export default HeaderBottom

// <nav>
// <ul>
//   <li><Link to='/'>Mon Profil</Link></li>
//   <li><Link to='/Albums'>Albums</Link></li>
//   <li><Link to='/Membres'>Membres</Link></li>
// </ul>
// </nav>